# -*- coding: utf-8 -*-

require 'benchmark'

class Hoge
  def self.class_method
      i = 0
  end
  def instance_method
      i = 0
  end
end

Benchmark.bm 20 do |r|
  ITERATION = 100000000
  local_variable = 0

  r.report "class method" do
    ITERATION.times do |i|
      Hoge.class_method
    end
  end

  r.report "instance method" do
    ITERATION.times do |i|
      Hoge.new.instance_method
    end
  end
end
