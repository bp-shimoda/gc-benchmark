# -*- coding: utf-8 -*-

from benchmarker import Benchmarker

class Hoge(object):
    
    @classmethod
    def class_method(cls):
        i = 0

    @staticmethod
    def static_method():
        i = 0

    def instance_method(self):
        i = 0


with Benchmarker(100000000, width = 20) as bench:
    @bench("class method")
    def _(bm):
        for i in bm:
            Hoge.class_method()

    @bench("static method")
    def _(bm):
        for i in bm:
            Hoge.static_method()

    @bench("instance method")
    def _(bm):
        for i in bm:
            Hoge().instance_method()
